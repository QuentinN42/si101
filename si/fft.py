from numbers import Number

import numpy as np


def fft(signal: np.ndarray, n: int) -> np.ndarray:
    return abs(np.fft.fft(signal, n=n))


# noinspection PyUnusedLocal
def gen_nus(n: int, freq_ech: Number = 0) -> np.ndarray:
    return np.linspace(0, 1, n)


def gen_fs(n: int, freq_ech: Number) -> np.ndarray:
    return freq_ech * gen_nus(n)


def fs_fft(signal: np.ndarray, n: int, freq_ech: Number):
    return gen_fs(n, freq_ech), fft(signal, n)


# noinspection PyUnusedLocal
def nus_fft(signal: np.ndarray, n: int, freq_ech: Number = 0):
    return gen_nus(n), fft(signal, n)
