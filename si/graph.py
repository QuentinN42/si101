from numbers import Number
from typing import Iterable

import matplotlib.pyplot as plt
import numpy as np
import plotly.express as px
import plotly.graph_objects as go

from .fft import nus_fft, fs_fft
from .signal import hanning, db, peaks


def circle_plot(angles: Iterable) -> None:
    ax = plt.axes()
    ax.plot(
        np.cos(np.linspace(0, 2 * np.pi, 1000)), np.sin(np.linspace(0, 2 * np.pi, 1000))
    )
    for angle in angles:
        ax.arrow(0, 0, np.cos(angle), np.sin(angle))
    plt.show()


def plot(x, y) -> None:
    px.line(x=x, y=y).show()


def scatters(dicos: Iterable) -> None:
    go.Figure(data=[go.Scatter(**dico) for dico in dicos]).show()


def param_plot(
    signal: np.ndarray,
    n: int,
    freq_ech: Number,
    _fr: bool = False,
    _db: bool = False,
    _ha: bool = False,
    _peaks: Number = None,
) -> None:
    if _fr:
        print("❌ f")
        print("✔ nu")
        fun = nus_fft
    else:
        print("✔ f")
        print("❌ nu")
        fun = fs_fft

    if _ha:
        print("✔ hanning")
        signal = hanning(signal)
    else:
        print("❌ hanning")

    f, s = fun(signal, n, freq_ech)

    if _db:
        print("✔ db")
        s = db(s)
    else:
        print("❌ db")
    
    if _peaks is not None:
        print("✔ peaks")
        p = peaks(s, _peaks)
        scatters([
            {"x": f, "y": s},
            {"x": f[p], "y": s[p], "mode": "markers"}
        ])
    else:
        print("❌ peaks")
        plot(f, s)
