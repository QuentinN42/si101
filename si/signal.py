from numbers import Number

import numpy as np
from scipy.signal import find_peaks


def gen_exp(a, f, start, stop, n, p):
    return a * np.exp(2j * np.pi * f * np.linspace(start, stop, n) + p)


def hanning(signal: np.ndarray) -> np.ndarray:
    return np.hanning(len(signal)) * signal


def db(signal) -> np.ndarray:
    return 20 * np.log10(signal)


# noinspection PyTypeChecker
def peaks(signal: np.ndarray, height: Number) -> np.ndarray:
    return find_peaks(signal, height)[0]
